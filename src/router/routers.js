export default [
  {
    path: '/',
    name: 'home',
    redirect: '/index',
    component: () => import('@/view/layout/home-layout.vue'),
    children: [
      {
        path: '/index',
        name: 'index',
        meta: {
          title: '首页',
          index: 0
        },
        component: () => import('@/view/index.vue')
      },
      {
        path: '/marker',
        name: 'marker',
        meta: {
          title: '书签',
          index: 1
        },
        component: () => import('@/view/marker.vue')
      },
      {
        path: '/weibo',
        name: 'weibo',
        meta: {
          title: '微博热搜',
          index: 2
        },
        component: () => import('@/view/weibo.vue')
      },
      {
        path: '/music163',
        name: 'music163',
        meta: {
          title: '云村热评',
          index: 3
        },
        component: () => import('@/view/music163.vue')
      }
    ]
  }
]

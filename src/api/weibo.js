import request from '@/libs/request'

let BASE_URL = 'https://api.hmister.cn'

export const getWeiboHotSearch = () => {
  return request({
    url: BASE_URL + '/weibo',
    method: 'get'
  })
};

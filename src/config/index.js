module.exports = {
    /**
     * token在Cookie中存储的天数，默认1天
     */
    cookieExpires: 1,
    /**
     * 项目部署基础
     * 默认情况下，我们假设你的应用将被部署在域的根目录下,
     * 例如：https://www.my-app.com/
     * 默认：'/'
     * 如果您的应用程序部署在子路径中，则需要在这指定子路径
     * 例如：https://www.foobar.com/my-app/
     * 需要将它改为'/my-app/'
     */
    publicPath: {
      //  本地环境发布目录
      dev: '/',
      //  生产环境发布目录
      pro: '/'
    },
    /**
     *  api请求基础路径
     */
    apiUrl: {
      //  本地环境接口请求地址
      dev: 'http://localhost:8900/',
      //  生产环境接口请求地址
      pro: 'http://localhost:8900/'
    },
    /**
     * 需要加载的插件
     */
    plugin: {

    }
  }
